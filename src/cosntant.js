const 404 = require('../index.json');

const random404 = () => {
  return 404[Math.floor(Math.random() * 404.length)];
};
module.exports = { 404, random404 };
