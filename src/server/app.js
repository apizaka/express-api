const swaggerUi = require('swagger-ui-express');
const swaggerFile = require('./swaggers.json');
const express = require('express');
const bodyParser = require('body-parser');
const expressip = require('express-ip');
const app = express();
const path = require('path');
function validateQuery(fields) {
	// eslint-disable-next-line consistent-return
	return (req, res, next) => {
		// eslint-disable-next-line no-restricted-syntax
		for (const field of fields) {
			if (!req.params[field]) {
				// Field isn't present, end request
				return res.status(400).send({ error: `${field} is missing` });
			}
		}

		next();
	};
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('view engine', 'ejs');
app.engine('.html', require('ejs').renderFile);
app.use(expressip().getIpInfoMiddleware);
var options = {
  explorer: true
};
const forwardedPrefixSwagger = async (req, res, next) => {
  req.originalUrl = (req.headers['x-forwarded-prefix'] || '') + req.url;
  next();
};

const root = require('path').join(__dirname, 'views');
app.use(express.static(root));
app.use('/api/docs/', swaggerUi.serve, swaggerUi.setup(swaggerFile, options));
//app.get("/api/poke/:id",async(t,a)=>{const i=`http://nginxt:80/details/${t.params.id}`,e=require("puppeteer"),o=await e.launch({args:["--window-size=1920,1080"]}),w=await o.newPage();await w.goto(`${i}`),await w.waitForTimeout(190),await w.setViewport({width:1200,height:1200});const n=await w.screenshot({type:"jpeg",quality:100,clip:{x:0,y:0,width:1200,height:1200},omitBackground:!0});await o.close(),a.contentType("image/jpeg");const p=await Buffer.from(n,"binary");await a.send(p)});
// app.use('/api/poke/:id', (req, res),express.static("views" + 'details/'+ req.params.id) );

//app.use('/api/poke/:id', (req, res),express.static("views" + '/'+ req.params.id) );


app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  );

  // Request headers you wish to allow
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type'
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});
app.get('/api/poke/:id',
validateQuery(['id']), async (req, res) => {
  //const req = require('@aero/http');
  const id = req.params.id;
  const api = `http://nginxt/details/${id}`;
  const puppeteer = require('puppeteer');
  const browser = await puppeteer.launch({ args: ['--no-sandbox'] });
  const page = await browser.newPage();
  await page.goto(`${api}`, {waitUntil: 'networkidle2'});
  // await page.waitForNavigation();
  await page.setViewport({
    width: 1200,
    height: 1200,
  });
  const imageBuffer = await page.screenshot({
    type: 'jpeg',
    quality: 100,
    clip: {
      x: 0,
      y: 0,
      width: 1200,
      height: 1200,
    },
    omitBackground: true,
  });

  await browser.close();



  res.contentType('image/jpeg');
  const buf = await Buffer.from(imageBuffer, 'binary');
  await res.send(buf);
})

require('./routes')(app);

module.exports = app;