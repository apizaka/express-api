const puppeteer = require('puppeteer');
module.exports = (app) => {
app.get('/api/poke/:id', async (req, res) => {
    //const req = require('@aero/http');
    const id = req.params.id;
    const api = `http://nginxt/details/${id}`;
    //const puppeteer = require('puppeteer');
    const browser = await puppeteer.launch({args: ['--no-sandbox']});
      const page = await browser.newPage();
      await page.goto(`${api}`);
      await page.waitForTimeout(190);
      await page.setViewport({
        width: 1200,
        height: 1200,
      });
      const imageBuffer = await page.screenshot({
        type: 'jpeg',
        quality: 100,
        clip: {
          x: 0,
          y: 0,
          width: 1200,
          height: 1200,
        },
        omitBackground: true,
      });
     
      await browser.close();
    
    

    res.contentType('image/jpeg');
      const buf = await Buffer.from(imageBuffer, 'binary');
     await res.send(buf);
        })
    };